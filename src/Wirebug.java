import java.io.*;
import java.nio.file.Files;
import java.net.InetAddress;

public class Wirebug{

    /*
        The readPacket method accepts the file and creates a byte array of hex values from it.
        It then appends all the values in string builder object. Removing the white spaces in between
        elements, a single string of entire hexdump is created.
        @param  filename    hexdump file
        @return String      trimmed hex value
     */
    private static String readPacket(String filename)
    {
        StringBuilder sb = new StringBuilder();

        try{
            byte[] byteArray = Files.readAllBytes(new File(filename).toPath());
            for (byte b : byteArray) {
                sb.append(String.format("%02X ", b));
            }
        }
        catch(Exception e)
        {
            System.out.println("Found an exception");
        }

        String hexString = sb.toString();
        String trimmedString = hexString.replaceAll("\\s+","");
        System.out.println(trimmedString);
        System.out.println();
        System.out.println("Removing the wireshark Header from the packet");
        System.out.println();
        return trimmedString.substring(80);
    }

    /*
        The readEthernetHeader method reads the ethernet header from the packet.
        It displays the packet size, sender and destination mac address, and type of
        next protocol.
        @param  String  packet hexdump
        @param  char[]  packet hexdump in array
        @return none    none
     */
    private static void readEthernetHeader(String actualPacket, char[] actualPacketChar)
    {
        System.out.println(":ETHER:     ---ETHER HEADER---");
        System.out.println(":ETHER:     Packet Size = "+(actualPacket.length()/2));
        System.out.println(":ETHER:     Destination = "+actualPacket.substring(0,2)+":"+actualPacket.substring(2,4)+":"+actualPacket.substring(4,6)+":"+actualPacket.substring(6,8)+":"+actualPacket.substring(8,10)+":"+actualPacket.substring(10,12));
        System.out.println(":ETHER:     Source      = "+actualPacket.substring(12,14)+":"+actualPacket.substring(14,16)+":"+actualPacket.substring(16,18)+":"+actualPacket.substring(18,20)+":"+actualPacket.substring(20,22)+":"+actualPacket.substring(22,24));
        if(actualPacketChar[25]=='8')
            System.out.println(":ETHER:     Type        = IPv4(0x0800)");
    }

    /*
        The method readIPHeader reads the Ip header of the packet. It fetches sender and receiver IP.
        Length, Header Checksum, Differentiated services, flags, identification and the underlying
        protocol are read from the hexdump file.
        @param  String  packet hexdump
        @param  char[]  packet hexdump as an array
        @return int     protocol value.
     */
    private static int readIPHeader (String actualPacket, char[] actualPacketChar)
    {
        int length;
        int decimalValue;
        int decimalValueOfServices;
        char[] binaryValueOfFlags = new char[16];
        char[] binaryValueOfServices = new char[8];
        for(int i=0;i<binaryValueOfFlags.length;i++)
            binaryValueOfFlags[i]='0';
        for(int i=0;i<binaryValueOfServices.length;i++)
            binaryValueOfServices[i]='0';
        System.out.println(":IP:        ---IP Header---");

        if(actualPacketChar[28]=='4')
        {
            System.out.println(":IP:        ..."+Integer.toBinaryString(Character.getNumericValue(actualPacketChar[28]))+"... Version: IPv4");
        }
        if(actualPacketChar[28]=='6')
            System.out.println(":IP:        ..."+Integer.toBinaryString(Character.getNumericValue(actualPacketChar[28]))+"... Version: IPv6");
        if(actualPacketChar[29]=='5')
            System.out.println(":IP:        ..."+Integer.toBinaryString(Character.getNumericValue(actualPacketChar[29]))+"... Header Length: 20 bytes");

        System.out.println(":IP:        Services: 0x"+actualPacket.substring(30,32));
        decimalValueOfServices = Integer.parseInt((actualPacket.substring(30,32)), 16);
        char[] tempServices = (Integer.toBinaryString(decimalValueOfServices)).toCharArray();
        int j = binaryValueOfServices.length;
        for (int i=tempServices.length-1; i>=0;i--)
            if(tempServices[i]=='0' || tempServices[i]=='1') {
                binaryValueOfServices[j - 1] = tempServices[i];
                j--;
            }
        String binaryStringOfServices = new String(binaryValueOfServices);
        switch (binaryStringOfServices.substring(0,6)){
            case "101110":
                System.out.println(":IP:            1011 10.. Expedited forwarding (EF) ");
                break;
            case "000000":
                System.out.println(":IP:            0000 00.. Best effort ");
                break;
            case "001010":
                System.out.println(":IP:            0010 10.. AF11    ");
                break;
            case "001100":
                System.out.println(":IP:            011 00.. AF12    ");
                break;
            case "001110":
                System.out.println(":IP:            0011 10.. AF13    ");
                break;
            case "010010":
                System.out.println(":IP:            0100 10.. AF21    ");
                break;
            case "010100":
                System.out.println(":IP:            0101 00.. AF22    ");
                break;
            case "010110":
                System.out.println(":IP:            0101 10.. AF23    ");
                break;
            case "011010":
                System.out.println(":IP:            0110 10.. AF31    ");
                break;
            case "011100":
                System.out.println(":IP:            0111 00.. AF32    ");
                break;
            case "011110":
                System.out.println(":IP:            0111 10.. AF33    ");
                break;
            case "100010":
                System.out.println(":IP:            1000 10.. AF41    ");
                break;
            case "100100":
                System.out.println(":IP:            1001 00.. AF42    ");
                break;
            case "100110":
                System.out.println(":IP:            1001 10.. AF43    ");
                break;
        }

        switch(binaryStringOfServices.substring(6)){
            case "00":
                System.out.println(":IP:            .... ..00 Non ECN-Capable Transport, Non-ECT");
                break;
            case "10":
                System.out.println(":IP:            .... ..10 ECN Capable Transport, ECT(0)");
                break;
            case "01":
                System.out.println(":IP:            .... ..01 ECN Capable Transport, ECT(1)");
                break;
            case "11":
                System.out.println(":IP:            .... ..11 Congestion Encountered, CE");
                break;
        }


        length = Integer.parseInt((actualPacket.substring(32,36)), 16);
        System.out.println(":IP:        Total Length: "+length);

        //identification
        System.out.println(":IP:        Identification: 0x"+actualPacket.substring(36,40));

        //flags
        System.out.println(":IP:        Flags: 0x"+actualPacket.substring(40,44));
        decimalValue = Integer.parseInt((actualPacket.substring(40,44)), 16);
        char[] temp = (Integer.toBinaryString(decimalValue)).toCharArray();
        int k = binaryValueOfFlags.length;
        for (int i=temp.length-1; i>=0;i--)
            if(temp[i]=='0' || temp[i]=='1') {
                binaryValueOfFlags[k - 1] = temp[i];
                k--;
            }
        for(int i=0;i<4;i++)
        {
            switch(i){
                case 0:
                    switch(binaryValueOfFlags[0]){
                    case '0':
                        System.out.println(":IP:            0... .... .... .... = Resereved Bit: Not Set");
                        break;
                    case '1':
                        System.out.println(":IP:            1... .... .... .... = Resereved Bit: Set");
                        break;
                }
                    break;
                case 1:
                    switch(binaryValueOfFlags[1]){
                        case '0':
                            System.out.println(":IP:            .0.. .... .... .... = Don't Fragment : Not Set");
                            break;
                        case '1':
                            System.out.println(":IP:            .1.. .... .... .... = Don't Fragment : Set");
                            break;
                    }
                    break;
                case 2:
                    switch(binaryValueOfFlags[2]){
                        case '0':
                            System.out.println(":IP:            ..0. .... .... .... = More Fragments : Not Set");
                            break;
                        case '1':
                            System.out.println(":IP:            ..1. .... .... .... = More Fragment : Set");
                            break;
                    }
                    break;
                case 3:
                    switch(binaryValueOfFlags[3]){
                        case '0':
                            System.out.println(":IP:            ...0 0000 0000 0000 = Fragment Offset : 0");
                            break;
                        case '1':
                            System.out.println(":IP:            ...1 0000 0000 0000 = Fragment Offset : 0");
                            break;
                    }
                    break;
            }
        }

        //time to live
        System.out.println(":IP:        Time to live : "+Integer.parseInt((actualPacket.substring(44,46)), 16));

        //Prototcol
        int protocol = Integer.parseInt((actualPacket.substring(46,48)), 16);
        switch(protocol){
            case 1:
                System.out.println(":IP:        Protocol: ICMP (01)");
                break;
            case 6:
                System.out.println(":IP:        Protocol: TCP (06)");
                break;
            case 17:
                System.out.println(":IP:        Protocol: UDP (11)");
                break;
        }

        //Header Checksum
        System.out.println(":IP:        Header Checksum: 0x"+actualPacket.substring(48,52));

        //Source IP
        try{
            String sourceIp = Integer.parseInt((actualPacket.substring(52,54)), 16)+"."+Integer.parseInt((actualPacket.substring(54,56)), 16)+"."+Integer.parseInt((actualPacket.substring(56,58)), 16)+"."+Integer.parseInt((actualPacket.substring(58,60)), 16);
            InetAddress sourceAddress = InetAddress.getByName(sourceIp);
            String sourceHost = sourceAddress.getHostName();
            System.out.println(":IP:        Source IP: "+sourceIp+",    "+sourceHost);
        }
        catch(Exception e) {
            System.out.println("Exception found");
        }



        //Destination IP
        try{
            String destinationIp = Integer.parseInt((actualPacket.substring(60,62)), 16)+"."+Integer.parseInt((actualPacket.substring(62,64)), 16)+"."+Integer.parseInt((actualPacket.substring(64,66)), 16)+"."+Integer.parseInt((actualPacket.substring(66,68)), 16);
            InetAddress destinationAddress = InetAddress.getByName(destinationIp);
            String destinationHost = destinationAddress.getHostName();
            System.out.println(":IP:        Destination IP: "+destinationIp+",  "+destinationHost);
        }
        catch(Exception e)
        {
            System.out.println("Exception found");
        }
        return protocol;

    }

    private static void readICMP(String actualPacket, char[] actualPacketChar)
    {
        System.out.println(":ICMP:      ---ICMP Header---");

        //Type
        int type = Integer.parseInt(actualPacket.substring(68,70),16);
        switch(type){
            case 0:
                System.out.println(":ICMP:      Echo Reply ("+type+")");
                break;
            case 3:
                System.out.println(":ICMP:      Destination Unreachable ("+type+")");
                break;
            case 4:
                System.out.println(":ICMP:      Source Quench ("+type+")");
                break;
            case 5:
                System.out.println(":ICMP:      Redirect ("+type+")");
                break;
            case 8:
                System.out.println(":ICMP:      Echo Request ("+type+")");
                break;
            case 9:
                System.out.println(":ICMP:      Router Advertisement ("+type+")");
                break;
            case 10:
                System.out.println(":ICMP:      Router Solicitation ("+type+")");
                break;
            case 11:
                System.out.println(":ICMP:      Time Exceeded ("+type+")");
                break;
            case 12:
                System.out.println(":ICMP:      Parameter Problem ("+type+")");
                break;
            case 13:
                System.out.println(":ICMP:      Timestamp Request ("+type+")");
                break;
            case 14:
                System.out.println(":ICMP:      Timestamp Reply ("+type+")");
                break;
            case 15:
                System.out.println(":ICMP:      Information Request ("+type+")");
                break;
            case 16:
                System.out.println(":ICMP:      Information Reply ("+type+")");
                break;
            case 17:
                System.out.println(":ICMP:      Address Mask Request ("+type+")");
                break;
            case 18:
                System.out.println(":ICMP:      Address Mask Reply ("+type+")");
                break;
        }

        //code
        System.out.println(":ICMP:      Code: "+Integer.parseInt(actualPacket.substring(70,72),16));

        //checksum
        System.out.println(":ICMP:      Checksum: 0x"+actualPacket.substring(72,76));
    }

    private static void readTCP(String actualPacket, char[] actualPacketChar)
    {
        char[] binaryValueOfFlagsInTCP = new char[16];
        for(int i=0;i<binaryValueOfFlagsInTCP.length;i++)
            binaryValueOfFlagsInTCP[i]='0';
        System.out.println(":TCP:       ---TCP Header---");
        //SourcePort
        System.out.println(":TCP:       Source Port: "+Integer.parseInt(actualPacket.substring(68,72), 16));

        //DestinationPort
        System.out.println(":TCP:       Destination Port: "+Integer.parseInt((actualPacket.substring(72,76)), 16));

        //Sequence number
        System.out.println(":TCP:       Sequence Number: "+Long.parseLong((actualPacket.substring(76,84)), 16));

        //Ackowledgement Number
        System.out.println(":TCP:       Ack Number: "+Long.parseLong((actualPacket.substring(84,92)), 16));
        int decimalValue = Integer.parseInt((actualPacket.substring(92,93)), 16);
        System.out.println(":TCP:       Header Length: "+decimalValue*32/8+" bytes.");
        //flags
        System.out.println(":IP:        Flags: 0x"+actualPacket.substring(92,96));
        decimalValue = Integer.parseInt((actualPacket.substring(92,96)), 16);
        char[] temp = (Integer.toBinaryString(decimalValue)).toCharArray();
        int j = binaryValueOfFlagsInTCP.length;
        for (int i=temp.length-1; i>=0;i--)
            if(temp[i]=='0' || temp[i]=='1') {
                binaryValueOfFlagsInTCP[j - 1] = temp[i];
                j--;
            }
        for(int i = 8; i<16; i++)
        {
            switch(i){
                case 8:
                    switch(binaryValueOfFlagsInTCP[8]){
                        case '0':
                            System.out.println(":TCP:       .... 0... .... = Congestion Window Reduced (CWR): Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... 1... .... = Congestion Window Reduced (CWR): Set");
                            break;
                    }
                    break;
                case 9:
                    switch(binaryValueOfFlagsInTCP[9]){
                        case '0':
                            System.out.println(":TCP:       .... .0.. .... = Nonce: Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... .1.. .... = Nonce: Set");
                            break;
                    }
                    break;
                case 10:
                    switch(binaryValueOfFlagsInTCP[10]){
                        case '0':
                            System.out.println(":TCP:       .... ..0. .... = Urgent: Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... ..1. .... = Urgent: Set");
                            break;
                    }
                    break;
                case 11:
                    switch(binaryValueOfFlagsInTCP[11]){
                        case '0':
                            System.out.println(":TCP:       .... ...0 .... = ACK: Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... ...1 .... = ACK: Set");
                            break;
                    }
                    break;
                case 12:
                    switch(binaryValueOfFlagsInTCP[12]){
                        case '0':
                            System.out.println(":TCP:       .... .... 0... = Push: Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... .... 1... = Push: Set");
                            break;
                    }
                    break;
                case 13:
                    switch(binaryValueOfFlagsInTCP[13]){
                        case '0':
                            System.out.println(":TCP:       .... .... .0.. = Reset: Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... .... .1.. = Reset: Set");
                            break;
                    }
                    break;
                case 14:
                    switch(binaryValueOfFlagsInTCP[14]){
                        case '0':
                            System.out.println(":TCP:       .... .... ..0. = SYN: Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... .... ..1. = SYN: Set");
                            break;
                    }
                    break;
                case 15:
                    switch(binaryValueOfFlagsInTCP[15]){
                        case '0':
                            System.out.println(":TCP:       .... .... ...0 = FIN: Not Set");
                            break;
                        case '1':
                            System.out.println(":TCP:       .... ..... ...1 = FIN: Set");
                            break;
                    }
                    break;
            }
        }
        //Window size
        System.out.println(":TCP:       Window Size: "+Long.parseLong((actualPacket.substring(96,100)), 16));

        //Checksum
        System.out.println(":TCP:       Checksum: 0x"+actualPacket.substring(100,104));

        //Urgent Pointer
        System.out.println(":TCP:       Urgent Pointer: "+Integer.parseInt((actualPacket.substring(104,108)), 16));

        //Options
        System.out.println(":TCP        No Options");

    }

    private static void readUDP(String actualPacket, char[] actualPacketChar)
    {
        System.out.println(":UDP:   ---UDP Header---");

        //source
        System.out.println(":UDP:   Source Port: "+Integer.parseInt(actualPacket.substring(68,72),16));

        //destination
        System.out.println(":UDP:   Destination Port: "+Integer.parseInt(actualPacket.substring(72,76),16));

        //length
        System.out.println(":UDP:   Length: "+Integer.parseInt(actualPacket.substring(76,80),16));

        //checksum
        System.out.println(":UDP:   Checksum: 0x"+actualPacket.substring(80,84));
    }

    /*
        The method of readProtocolHeader gives the control to the specific method of protocol.
        Here we have just taken into account ICMP, TCP and UDP protocols.
        @param int  the protocol value calculated by IPHeader
        @param String   packet hexdumo
        @param char[]   packet hexdump as an array
        @return none    none
     */
    private static void readProtocolHeader(int protocol, String actualPacket, char[] actualPacketChar)
    {
        switch(protocol){
            case 1:
                readICMP(actualPacket,actualPacketChar);
                break;
            case 6:
                readTCP(actualPacket,actualPacketChar);
                break;
            case 17:
                readUDP(actualPacket,actualPacketChar);
                break;
        }
    }

    private static void decodePayload(String actualPacket)
    {

        String hexString = actualPacket.substring(132);
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < hexString.length(); i += 2) {
            String str = hexString.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        System.out.println();
        System.out.println(":Payload:   ---Payload---");
        System.out.println(output.toString());
    }

    /*
     * The main method encompasses several methods to read the Ethernet Header, IP Header & Protocol header.
     * It utilizes concepts of char array, string manipulations, hex:binary:decimal conversions, network
     * programming.
     *
     * @param   command-line
     * @return none
     */
    public static void main(String[] args)
    {
        System.out.println("Welcome to Wirebug, Let us analyze some packets");
        //calling the method readPacket
        String actualPacket = readPacket(args[0]);
        char[] actualPacketChar = actualPacket.toCharArray();
        System.out.println("The actual packet is "+actualPacket);
        System.out.println();
        //calling the method to read ethernet header
        readEthernetHeader(actualPacket, actualPacketChar);
        System.out.println();
        int protocol = readIPHeader(actualPacket, actualPacketChar);
        System.out.println();
        readProtocolHeader(protocol, actualPacket, actualPacketChar);
        if (actualPacket.length()>132)
            decodePayload(actualPacket);
    }
}